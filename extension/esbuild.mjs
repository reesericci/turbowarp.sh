import * as esbuild from 'esbuild'
import * as importMap from "esbuild-plugin-import-map";
import packageJson from "./package.json" with { type: 'json' };

await esbuild.build({
  entryPoints: ['src/index.js'],
  bundle: true,
  outfile: 'dist/bundle.min.js',
  format: "iife",
  target: "es2020",
  sourcemap: true,
  loader: { '.wasm': 'binary' },
  banner: {
    js: `// Name: turbowarp.sh
// By: reesericci
// ID: ssh
// Description: Connect to SSH servers over WebSockets!
// License: LGPL-3.0-or-later

// Full git repository at https://sr.ht/~reesericci/turbowarp.sh
`
  }
})

importMap.load({
  imports: {
      '@turbowarp.sh/client': `https://cdn.jsdelivr.net/npm/@turbowarp.sh/client@${packageJson.dependencies['@turbowarp.sh/client']}/dist/index.js`
  }
})

await esbuild.build({
  entryPoints: ['src/index.js'],
  bundle: true,
  outfile: 'dist/netloaded.min.js',
  format: "iife",
  target: "esnext",
  supported: {
   "dynamic-import": true 
  },
  external: ["@turbowarp.sh/client"],
  platform: "browser",
  plugins: [importMap.plugin()],
  sourcemap: true,
  loader: { '.wasm': 'binary' },
  banner: {
    js: `// Name: turbowarp.sh
// By: reesericci
// ID: ssh
// Description: Connect to SSH servers over WebSockets!
// License: LGPL-3.0-or-later

// Full git repository at https://sr.ht/~reesericci/turbowarp.sh
`
  }
})