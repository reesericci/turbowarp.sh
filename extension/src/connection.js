export default class Connection {
  constructor(username, host, identity, proxy) {
    this.username = username
    this.host = host
    this.identity = identity
    this.proxy = proxy || this.host

  }

  connect() {
    return goConnect(this.username, this.host, this.identity, this.proxy) // eslint-disable-line no-undef
  }

  toString() {
    return JSON.stringify({
      username: this.username,
      host: this.host,
      identity: this.identity,
      proxy: this.proxy
    })
  }

  toFriendlyString() {
    return `${this.username}@${this.host} via ${this.proxy}`
  }

  static parse(encoded) {
    let parsed = JSON.parse(encoded)
    return new Connection(parsed.username, parsed.host, parsed.identity, parsed.proxy)
  }
}