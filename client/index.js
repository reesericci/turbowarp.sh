import wasm from "./dist/main.wasm"
import Go from "./go_loader"

const go = new Go()

export default class Client {
    constructor() {
      WebAssembly.instantiate(wasm, go.importObject).then((result) => {
        go.run(result.instance);
      });
    }
}