package main

import (
	"bytes"
	"context"
	"log"
	"net"
	"sync"
	"syscall/js"
	"time"

	"golang.org/x/crypto/ssh"
	"nhooyr.io/websocket"
)

type Connection struct {
	nc      net.Conn
	wc 			*websocket.Conn
	client  *ssh.Client
}

var activeConnection Connection

func connectSSH(username, host, identity, proxy string, resolve js.Value, reject js.Value) error {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()


	log.Println("Dialing WebSocket...")
	dialed, _, err := websocket.Dial(ctx, proxy, nil)
	if err != nil {
		log.Printf("Failed to dial WebSocket: %v", err)
		return err
	}

	dialed.Ping(ctx)

	log.Println("WebSocket connection dialed")

	c := websocket.NetConn(context.Background(), dialed, websocket.MessageBinary)
	log.Println("NetConn created")

	signer, err := ssh.ParsePrivateKey([]byte(identity))
	if err != nil {
		reject.Invoke(err.Error())
		return err
	}

	ncc, chans, reqs, err := ssh.NewClientConn(c, host, &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	})
	if err != nil {
		reject.Invoke(err.Error())
		return err
	}

	client := ssh.NewClient(ncc, chans, reqs)

	activeConnection = Connection{
		nc:      c,
		client:  client,
		wc: dialed,
	}

	resolve.Invoke()
	return nil
}

func execute(command string, resolve js.Value, reject js.Value) {

	if activeConnection == (Connection{}) {
		reject.Invoke("no active connection")
		return
	}

	session, err := activeConnection.client.NewSession()
	if err != nil {
		reject.Invoke(err.Error())
		return
	}

	var b bytes.Buffer
	session.Stdout = &b

	if err := session.Run(command); err != nil {
		reject.Invoke(err.Error())
	}

	resolve.Invoke(b.String())
}

func connectSSHWrapper() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// Handler for the Promise: this is a JS function
		// It receives two arguments, which are JS functions themselves: resolve and reject

		if len(args) < 4 {
			log.Println("Insufficient arguments")
			return nil
		}
	
		username := args[0].String()
		host := args[1].String()
		identity := args[2].String()
		proxy := args[3].String()

		handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			resolve := args[0]
			reject := args[1]

			// Now that we have a way to return the response to JS, spawn a goroutine
			// This way, we don't block the event loop and avoid a deadlock
			go connectSSH(username, host, identity, proxy, resolve, reject)

			// The handler of a Promise doesn't return any value
			return nil
		})

		// Create and return the Promise object
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	})
}

func executeWrapper() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// Handler for the Promise: this is a JS function
		// It receives two arguments, which are JS functions themselves: resolve and reject

		if len(args) < 1 {
			log.Println("Insufficient arguments")
			return nil
		}
	
		command := args[0].String()

		handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			resolve := args[0]
			reject := args[1]

			// Now that we have a way to return the response to JS, spawn a goroutine
			// This way, we don't block the event loop and avoid a deadlock
			go execute(command, resolve, reject)

			// The handler of a Promise doesn't return any value
			return nil
		})

		// Create and return the Promise object
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	})
}

func disconnectWrapper() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// Handler for the Promise: this is a JS function
		// It receives two arguments, which are JS functions themselves: resolve and reject
	
		handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			resolve := args[0]
			reject := args[1]

			// Now that we have a way to return the response to JS, spawn a goroutine
			// This way, we don't block the event loop and avoid a deadlock
			go func(resolve, reject js.Value) {
				if activeConnection != (Connection{}) {
					activeConnection.nc.Close()
					activeConnection = Connection{}
					resolve.Invoke()
				} else {
					reject.Invoke("no active connection")
				}
			}(resolve, reject)

			// The handler of a Promise doesn't return any value
			return nil
		})

		// Create and return the Promise object
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	})
}

func connectedWrapper() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// Handler for the Promise: this is a JS function
		// It receives two arguments, which are JS functions themselves: resolve and reject
	
		handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			resolve := args[0]

			// Now that we have a way to return the response to JS, spawn a goroutine
			// This way, we don't block the event loop and avoid a deadlock
			go func(resolve js.Value) {
				if activeConnection != (Connection{}) {
					ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
					defer cancel()
					err := activeConnection.wc.Ping(ctx)
					if err != nil {
						resolve.Invoke(false)
					} else {
						resolve.Invoke(true)
					}
				} else {
					resolve.Invoke(false)
				}
			}(resolve)

			// The handler of a Promise doesn't return any value
			return nil
		})

		// Create and return the Promise object
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	})
}

func main() {
	println("SSHKit Initialized")

	js.Global().Set("goConnect", connectSSHWrapper())
	js.Global().Set("goExecute", executeWrapper())
	js.Global().Set("goDisconnect", disconnectWrapper())
	js.Global().Set("goConnected", connectedWrapper())

	var wg sync.WaitGroup

	wg.Add(1)
	wg.Wait()
}
