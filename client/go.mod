module reeseric.ci/turbowarp.sh/client/v2

go 1.22.1

require nhooyr.io/websocket v1.8.10

require (
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/gopherjs/websocket v0.0.0-20191103002815-9a42957e2b3a // indirect
	github.com/tarndt/wasmws v0.0.0-20211231175046-02849cc2d4d2 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
